<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Data</title>
</head>
<body>
    <h1>Form PHP</h1>
    
    <form action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
        <label for="name">Name</label>
        <input type="text" name="name" id="name" placeholder="Input your name">
        <br>
        <label for="nim">NIM</label>
        <input type="text" name="nim" id="nim" placeholder="Input your nim">
        <br>
        <label for="prodi">Pilih Prodi</label>
        <select name="prodi" id="prodi">
            <option value="jurusan" disabled selected>Pilih Jurusan</option>
            <option value="Ilmu Komputer">Ilmu Komputer</option>
            <option value="Teknik Komputer">Teknik Komputer</option>
            <option value="Teknik Informatika">Teknik Informatika</option>
            <option value="Teknologi Informasi">Teknologi Informasi</option>
            <option value="Sistem Informasi">Sistem Informasi</option>
            <option value="Pendidikan Teknologi Informasi">Pendidikan Teknologi Informasi</option>
        </select>
        <br>
        <input type="radio" name="gender" id="gender-lk" value="Laki-Laki">
        <label for="gender-lk">Laki-Laki</label>
        <input type="radio" name="gender" id="gender-pr" value="Perempuan">
        <label for="gender-pr">Perempuan</label>
        <br>
        <input type="submit" value="Kirim">
    </form>
    <br>
    <h1>=======================RESULT=======================</h1>
    <?php
        if ($_SERVER['REQUEST_METHOD'] == "POST"){
            if(isset($_POST['prodi'], $_POST['name'], $_POST['nim'], $_POST['gender'])) {
                $name = $_POST['name'];
                $nim = $_POST['nim'];
                $prodi = $_POST['prodi'];
                $gender = $_POST['gender'];

                echo "Nama : " . $name . "<br>";
                echo "NIM : " . $nim . "<br>";
                echo "Prodi : " . $prodi . "<br>";
                echo "Gender : " . $gender . "<br>";

            }
        }
    ?>
</body>
</html>